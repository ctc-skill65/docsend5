<?php

return [
    'app_name' => 'ระบบส่งเอกสารออนไลน์',
    'site_url' => 'http://skill65.local/docSend5',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_docSend5',
    'db_charset' => 'utf8mb4'
];
