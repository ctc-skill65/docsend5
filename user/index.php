<?php
require_once __DIR__ . '/../boot.php';
checkAuth('user');

ob_start();
?>
<h1>ยินดีต้อนรับ <?= $user['firstname'] . ' ' . $user['lastname'] ?></h1>
<?php
$layout_page = ob_get_clean();
$page_name = 'หน้าหลัก';
require ROOT . '/user/layout.php';
