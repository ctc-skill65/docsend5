<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');
$dept_id = get('id');
$page_path = "/user/profile/edit.php";

if ($_POST) {
    $email = post('email');
    $check = DB::row("SELECT * FROM `users` WHERE `email`='{$email}' AND `user_id`!='{$user_id}'");
    if (!empty($check)) {
        setAlert('error', "มีอีเมล {$email} แล้วไม่สามารถใช้ซ้ำได้");
        redirect($page_path);
    }

    $result = DB::update('users', [
        'firstname' => post('firstname'),
        'lastname' => post('lastname'),
        'email' => $email,
        'dept_id' => post('dept_id')
    ], "`user_id`='{$user_id}'");

    if ($result) {
        setAlert('success', "แก้ไขข้อมูลส่วนตัวสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขข้อมูลส่วนตัวได้");
    }

    redirect($page_path);
}

$items = DB::result("SELECT * FROM `depts`");
ob_start();
?>
<?= showAlert() ?>
<form method="post">
    <label for="firstname">ชื่อ</label>
    <input type="text" name="firstname" id="firstname" value="<?= $user['firstname'] ?>" required>
    <br>

    <label for="lastname">นามสกุล</label>
    <input type="text" name="lastname" id="lastname" value="<?= $user['lastname'] ?>" required>
    <br>

    <label for="email">อีเมล</label>
    <input type="email" name="email" id="email" value="<?= $user['email'] ?>" required>
    <br>

    <label for="dept_id">แผนก</label>
    <select name="dept_id" id="dept_id">
        <option value="" selected disabled> ---- เลือก ---- </option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['dept_id'] ?>" <?= $item['dept_id'] === $user['dept_id'] ? 'selected' : null ?>><?= $item['dept_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขข้อมูลส่วนตัว';
require ROOT . '/user/layout.php';
