<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/sends/dept.php";

if ($_POST) {
    $file = upload('file');
    if (!$file) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดไฟล์ได้");
        redirect($page_path);
    }

    $result = DB::insert('docs', [
        'user_id' => $user_id,
        'doc_type_id' => post('doc_type_id'),
        'doc_name' => post('doc_name'),
        'file' => $file,
        'send_type' => 'dept',
        'to_dept_id' => post('to_dept_id'),
        'read_status' => 0,
        'dowload' => 0,
        'send_time' => date(DATE_SQL)
    ]);

    if ($result) {
        setAlert('success', "ส่งเอกสารสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถส่งเอกสารได้");
    }

    redirect($page_path);
}

$doc_types = DB::result("SELECT * FROM `doc_types`");
$items = DB::result("SELECT * FROM `depts`");
ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="doc_name">ชื่อเอกสาร</label>
    <input type="text" name="doc_name" id="doc_name" required>
    <br>

    <label for="file">ไฟล์เอกสาร</label>
    <input type="file" name="file" id="file" required>
    <br>

    <label for="doc_type_id">ประเภทเอกสาร</label>
    <select name="doc_type_id" id="doc_type_id" required>
        <option value="" selected disabled> ---- เลือก ---- </option>
        <?php foreach ($doc_types as $item) : ?>
            <option value="<?= $item['doc_type_id'] ?>"><?= $item['doc_type_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <label for="to_dept_id">ส่งถึง</label>
    <select name="to_dept_id" id="to_dept_id" required>
        <option value="" selected disabled> ---- เลือก ---- </option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['dept_id'] ?>"><?= $item['dept_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'ส่งเอกสารให้แผนกหรืองานต่างๆ';
require ROOT . '/user/layout.php';
