-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2023 at 11:00 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skill65_docsend5`
--

-- --------------------------------------------------------

--
-- Table structure for table `depts`
--

DROP TABLE IF EXISTS `depts`;
CREATE TABLE `depts` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `depts`
--

INSERT INTO `depts` (`dept_id`, `dept_name`) VALUES
(4, 'ฝ่ายการตลาด'),
(6, 'ฝ่ายบุคคล');

-- --------------------------------------------------------

--
-- Table structure for table `docs`
--

DROP TABLE IF EXISTS `docs`;
CREATE TABLE `docs` (
  `doc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `doc_type_id` int(11) NOT NULL,
  `doc_name` varchar(60) NOT NULL,
  `file` varchar(100) NOT NULL,
  `send_type` enum('dept','user') NOT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `to_dept_id` int(11) DEFAULT NULL,
  `read_status` int(1) NOT NULL,
  `dowload` int(11) NOT NULL,
  `send_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `docs`
--

INSERT INTO `docs` (`doc_id`, `user_id`, `doc_type_id`, `doc_name`, `file`, `send_type`, `to_user_id`, `to_dept_id`, `read_status`, `dowload`, `send_time`) VALUES
(1, 3, 1, 'รายงาน', '/storage/63d0fbbf28b5a.jpg', 'user', 4, NULL, 0, 0, '2023-01-25 16:51:59'),
(2, 3, 2, 'ทักษะเทคโนโลยีเครือข่าย 2565-2567', '/storage/63d0fbe0a07aa.pdf', 'user', 4, NULL, 0, 0, '2023-01-25 16:52:32'),
(3, 3, 2, 'ทักษะเทคโนโลยีเครือข่าย 2565-2567', '/storage/63d0fd3583d56.pdf', 'dept', NULL, 6, 0, 0, '2023-01-25 16:58:13');

-- --------------------------------------------------------

--
-- Table structure for table `doc_types`
--

DROP TABLE IF EXISTS `doc_types`;
CREATE TABLE `doc_types` (
  `doc_type_id` int(11) NOT NULL,
  `doc_type_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `doc_types`
--

INSERT INTO `doc_types` (`doc_type_id`, `doc_type_name`) VALUES
(1, 'เอกสารทั่วไป'),
(2, 'เอกสารฉบับสมบูรณ์'),
(3, 'หนังสือภายใน');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `user_type` enum('admin','user') NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `email`, `password`, `dept_id`, `user_type`, `status`) VALUES
(1, 'admin', 'demo', 'admin@demo.com', '25d55ad283aa400af464c76d713c07ad', 0, 'admin', 1),
(3, 'user', 'demo', 'user@demo.com', '25d55ad283aa400af464c76d713c07ad', 4, 'user', 1),
(4, 'user2', 'demo', 'user2@demo.com', '25d55ad283aa400af464c76d713c07ad', 0, 'user', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `depts`
--
ALTER TABLE `depts`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `docs`
--
ALTER TABLE `docs`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `doc_types`
--
ALTER TABLE `doc_types`
  ADD PRIMARY KEY (`doc_type_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `depts`
--
ALTER TABLE `depts`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `docs`
--
ALTER TABLE `docs`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `doc_types`
--
ALTER TABLE `doc_types`
  MODIFY `doc_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
