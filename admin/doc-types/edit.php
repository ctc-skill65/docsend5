<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$doc_type_id = get('id');
$page_path = "/admin/doc-types/edit.php?id={$doc_type_id}";

if ($_POST) {
    $result = DB::update('doc_types', [
        'doc_type_name' => post('doc_type_name')
    ], "`doc_type_id`='{$doc_type_id}'");

    if ($result) {
        setAlert('success', "แก้ไขประเภทเอกสารสำเร็จเรียบร้อย");
        redirect('/admin/doc-types/list.php');
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขประเภทเอกสารได้");
    }

    redirect($page_path);
}

$data = DB::row("SELECT * FROM `doc_types` WHERE `doc_type_id`='{$doc_type_id}'");
ob_start();
?>
<?= showAlert() ?>

<form method="post">
    <label for="doc_type_name">ชื่อประเภทเอกสาร</label>
    <input type="text" name="doc_type_name" id="doc_type_name" value="<?= $data['doc_type_name'] ?>" required>
    <button type="submit">บันทึก</button>
</form>

<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขประเภทเอกสาร';
require ROOT . '/admin/layout.php';
