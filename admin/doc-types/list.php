<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/doc-types/list.php";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        DB::delete('doc_types', "`doc_type_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

if ($_POST) {
    $result = DB::insert('doc_types', [
        'doc_type_name' => post('doc_type_name')
    ]);

    if ($result) {
        setAlert('success', "เพิ่มประเภทเอกสารสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มประเภทเอกสารได้");
    }

    redirect($page_path);
}

$items = DB::result("SELECT * FROM `doc_types`");
ob_start();
?>
<?= showAlert() ?>

<h3>เพิ่มประเภทเอกสาร</h3>
<form method="post">
    <label for="doc_type_name">ชื่อประเภทเอกสาร</label>
    <input type="text" name="doc_type_name" id="doc_type_name" required>
    <button type="submit">บันทึก</button>
</form>

<h3>รายการประเภทเอกสาร</h3>
<table>
    <thead>
        <tr> 
            <th>รหัส</th>
            <th>ชื่อประเภทเอกสาร</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['doc_type_id'] ?></td>
                <td><?= $item['doc_type_name'] ?></td>  
                <td>
                    <a href="<?= url("/admin/doc-types/edit.php?id={$item['doc_type_id']}") ?>">
                    แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['doc_type_id'] ?>"
                    <?= clickConfirm("คุณต้องการลบ {$item['doc_type_name']} หรือไม่") ?>
                    >
                    ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการประเภทเอกสาร';
require ROOT . '/admin/layout.php';
