<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/depts/list.php";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        DB::delete('depts', "`dept_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

if ($_POST) {
    $result = DB::insert('depts', [
        'dept_name' => post('dept_name')
    ]);

    if ($result) {
        setAlert('success', "เพิ่มแผนกหรืองานต่างๆสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มแผนกหรืองานต่างๆได้");
    }

    redirect($page_path);
}

$items = DB::result("SELECT * FROM `depts`");
ob_start();
?>
<?= showAlert() ?>

<h3>เพิ่มแผนกหรืองานต่างๆ</h3>
<form method="post">
    <label for="dept_name">ชื่อแผนกหรืองานต่างๆ</label>
    <input type="text" name="dept_name" id="dept_name" required>
    <button type="submit">บันทึก</button>
</form>

<h3>รายการแผนกหรืองานต่างๆ</h3>
<table>
    <thead>
        <tr> 
            <th>รหัส</th>
            <th>ชื่อแผนกหรืองานต่างๆ</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['dept_id'] ?></td>
                <td><?= $item['dept_name'] ?></td>  
                <td>
                    <a href="<?= url("/admin/depts/edit.php?id={$item['dept_id']}") ?>">
                    แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['dept_id'] ?>"
                    <?= clickConfirm("คุณต้องการลบ {$item['dept_name']} หรือไม่") ?>
                    >
                    ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการแผนกหรืองานต่างๆ';
require ROOT . '/admin/layout.php';
