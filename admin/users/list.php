<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/users/list.php";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'approve':
        DB::update('users', [
            'status' => 1
        ], "`user_id`='{$id}'");
        break;

    case 'cancel':
        DB::update('users', [
            'status' => -1
        ], "`user_id`='{$id}'");
        break;

    case 'delete':
        DB::delete('users', "`user_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

$items = DB::result("SELECT * FROM `users` WHERE `user_type`='user'");
ob_start();
?>
<?= showAlert() ?>

<table>
    <thead>
        <tr> 
            <th>รหัส</th>
            <th>ชื่อ</th>
            <th>นามสกุล</th>
            <th>อีเมล</th>
            <th>สถานะ</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['user_id'] ?></td>
                <td><?= $item['firstname'] ?></td>
                <td><?= $item['lastname'] ?></td>
                <td><?= $item['email'] ?></td>
                <td>
                    <?php
                    switch ($item['status']) {
                        case '-1':
                            echo "ระงับการใช้งาน";
                            break;

                        case '1':
                            echo "ใช้งาน";
                            break;

                        case '0':
                            echo "ขอใช้งาน";
                            break;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    switch ($item['status']) {
                        case '-1':
                            ?>
                            <a href="?action=approve&id=<?= $item['user_id'] ?>"
                            <?= clickConfirm("คุณต้องการยกเลิกระงับการใช้งาน {$item['email']} หรือไม่") ?>
                            >
                            ยกเลิกระงับการใช้งาน
                            </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="?action=delete&id=<?= $item['user_id'] ?>"
                            <?= clickConfirm("คุณต้องการลบ {$item['email']} หรือไม่") ?>
                            >
                            ลบ
                            </a>
                            <?php
                            break;

                        case '1':
                            ?>
                            <a href="?action=cancel&id=<?= $item['user_id'] ?>"
                            <?= clickConfirm("ระงับการใช้งาน {$item['email']} หรือไม่") ?>
                            >
                            ระงับการใช้งาน
                            </a>
                            <?php
                            break;

                        case '0':
                            ?>
                            <a href="?action=approve&id=<?= $item['user_id'] ?>"
                            <?= clickConfirm("คุณต้องการอนุญาต {$item['email']} หรือไม่") ?>
                            >
                            อนุญาต
                            </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="?action=delete&id=<?= $item['user_id'] ?>"
                            <?= clickConfirm("คุณต้องการลบ {$item['email']} หรือไม่") ?>
                            >
                            ลบ
                            </a>
                            <?php
                            break;
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการข้อมูลผู้ใช้งานระบบ';
require ROOT . '/admin/layout.php';
