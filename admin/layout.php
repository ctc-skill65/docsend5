<?php
ob_start();
?>
<h1><?= conf('app_name') ?></h1>
<hr>

<h1><?= isset($page_name) ? $page_name : null ?></h1>

<h3><?= $user['firstname'] . ' ' . $user['lastname'] ?> (ผู้ดูแลระบบ)</h3>

<nav>
    <h3>เมนู</h3>
    <ul>
        <li><a href="<?= url('/admin/index.php') ?>">หน้าหลัก</a></li>
        <li><a href="<?= url('/admin/users/list.php') ?>">จัดการข้อมูลผู้ใช้งานระบบ</a></li>
        <li><a href="<?= url('/admin/doc-types/list.php') ?>">จัดการประเภทเอกสาร</a></li>
        <li><a href="<?= url('/admin/depts/list.php') ?>">จัดการแผนกหรืองานต่างๆ</a></li>
        <li><a href="<?= url('/admin/docs/list.php') ?>">ข้อมูลการส่งเอกสาร</a></li>
        <li>ข้อมูลส่วนตัว
            <ul>
                <li><a href="<?= url('/admin/profile/edit.php') ?>">แก้ไขข้อมูลส่วนตัว</a></li>
                <li><a href="<?= url('/admin/profile/edit-pass.php') ?>">แก้ไขรหัสผ่าน</a></li>
                <li><a href="<?= url('/auth/logout.php') ?>" <?= clickConfirm('คุณต้องการออกจากระบบหรือไม่') ?>>ออกจากระบบ</a></li>
            </ul>
        </li>
    </ul>
</nav>

<main>
    <?= isset($layout_page) ? $layout_page : null ?>
</main>
<?php
$layout_body = ob_get_clean();
require INC . '/base_layout.php';