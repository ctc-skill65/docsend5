<?php
require_once __DIR__ . '/../boot.php';
checkLogin();

$error = 'Not Found';

$id = get('id');
if (empty($id)) {
    exit($error);
}

$doc = DB::row("SELECT * FROM `docs` WHERE `doc_id`='$id'");
if (empty($doc)) {
    exit($error);
}

if ($doc['to_user_id'] === $user_id || $doc['to_dept_id'] === $user['dept_id']) {
    DB::update('docs', [
        'read_status' => 1
    ], "`doc_id`='{$id}'");
}

DB::query("UPDATE `docs` SET `dowload`=`dowload`+1 WHERE `doc_id`='{$id}'");

redirect($doc['file']);

echo $error;
