<?php
require_once __DIR__ . '/../boot.php';

$page_path = "/auth/login.php";

if ($_POST) {
    $email = post('email');
    $hash = md5(post('password'));
    $user = DB::row("SELECT * FROM `users` WHERE `email`='{$email}' AND `password`='{$hash}'");
    if (empty($user)) {
        setAlert('error', "อีเมลหรือรหัสผ่านไม่ถูกต้อง");
        redirect($page_path);
    }

    switch ($user['status']) {
        case '-1':
            setAlert('error', "บัญชีถูกระงับการใช้งาน");
            redirect($page_path);
            break;

        case '0':
            setAlert('error', "บัญชีอยู่ระหว่างขออนุญาตใช้งาน");
            redirect($page_path);
            break;
    }

    $_SESSION['user_id'] = $user['user_id'];
    redirect("/{$user['user_type']}/index.php");
}

ob_start();
?>
<h1><?= conf('app_name') ?></h1>
<hr>

<h1>เข้าสู่ระบบ</h1>

<?= showAlert() ?>
<form method="post">
    <label for="email">อีเมล</label>
    <input type="email" name="email" id="email" required>
    <br>

    <label for="password">รหัสผ่าน</label>
    <input type="password" name="password" id="password" required>
    <br>

    <button type="submit">เข้าสู่ระบบ</button>
</form>

<p>
    ยังไม่มีบัญชี? <a href="<?= url('/auth/register.php') ?>">สมัครสมาชิก</a>
</p>

<?php
$layout_body = ob_get_clean();
$page_name = 'เข้าสู่ระบบ';
require INC . '/base_layout.php';
