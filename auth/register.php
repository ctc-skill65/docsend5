<?php
require_once __DIR__ . '/../boot.php';

$page_path = "/auth/register.php";

if ($_POST) {
    $email = post('email');
    $check = DB::row("SELECT * FROM `users` WHERE `email`='{$email}'");
    if (!empty($check)) {
        setAlert('error', "มีอีเมล {$email} แล้วไม่สามารถสมัครซ้ำได้");
        redirect($page_path);
    }

    $result = DB::insert('users', [
        'firstname' => post('firstname'),
        'lastname' => post('lastname'),
        'email' => $email,
        'password' => md5(post('password')),
        'dept_id' => post('dept_id'),
        'user_type' => 'user',
        'status' => 0
    ]);

    if ($result) {
        setAlert('success', "สมัครสมาชิกสำเร็จเรียบร้อย บัญชีอยู่ระหว่างขออนุญาตใช้งาน");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถสมัครสมาชิกได้");
    }

    redirect($page_path);
}

$items = DB::result("SELECT * FROM `depts`");
ob_start();
?>
<h1><?= conf('app_name') ?></h1>
<hr>

<h1>สมัครสมาชิก</h1>

<?= showAlert() ?>
<form method="post">
    <label for="firstname">ชื่อ</label>
    <input type="text" name="firstname" id="firstname" required>
    <br>

    <label for="lastname">นามสกุล</label>
    <input type="text" name="lastname" id="lastname" required>
    <br>

    <label for="email">อีเมล</label>
    <input type="email" name="email" id="email" required>
    <br>

    <label for="password">รหัสผ่าน</label>
    <input type="password" name="password" id="password" required>
    <br>

    <label for="dept_id">แผนก</label>
    <select name="dept_id" id="dept_id">
        <option value="" selected disabled> ---- เลือก ---- </option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['dept_id'] ?>"><?= $item['dept_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <button type="submit">สมัครสมาชิก</button>
</form>

<p>
    มีบัญชีแล้ว? <a href="<?= url('/auth/login.php') ?>">เข้าสู่ระบบ</a>
</p>

<?php
$layout_body = ob_get_clean();
$page_name = 'สมัครสมาชิก';
require INC . '/base_layout.php';
